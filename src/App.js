import React from 'react';
import AppRouter from './AppRouter';
import AppRouterParams from './AppRouterParam';
import AppRouterAuth from './AppRouterAuth';


function App() {
  return (
    <div>
      <AppRouterAuth/>
        <hr/>
      <AppRouterParams/>
      <hr/>
      <AppRouter></AppRouter>
    </div>
  );
}

export default App;
