import React from 'react';
import {BrowserRouter  as Router,Route,Link,Redirect,withRouter} from 'react-router-dom'

function AppRouterAuth(){
  return(
    <Router>
      <div>
        <AuthButton/>
          <ul>
            <li>
              <Link to="/public">Public Page</Link>
            </li>
            <li>
              <Link to="/protected">Protected page</Link>
            </li>
          </ul>
          <Route path="/public" component={Public}/>
          <Route path="/login" component={Login}/>
          <PrivateRoute path="/protected" component={Protected}/>
      </div>
    </Router>
  );
}

const fakeAuth = {
  isAutenticated:false,
  authenticate(cb){
    this.isAutenticated=true;
    setTimeout(cb,100);
  },
  signout(cb){
    this.isAutenticated=false;
    setTimeout(cb,100);
  }
};

const AuthButton = withRouter(
  ({history}) =>  fakeAuth.isAutenticated?(
    <p>
      welcome!{" "}
      <button
          onClick={()=> {
            fakeAuth.signout(() => history.push("/"))
          }}
      >
        signout
          </button>
          </p>
  ):(<p> you are logged in.</p>)
);

function PrivateRoute ({component:Component,...rest}){
  return(
    <Route {...rest} render={props => fakeAuth.isAutenticated?(
      <Component{...props}/>

    ):(
      <Redirect to = {{
        pathname:"/login",
        state:{from:props.location}
      }}
      />
    )}
    />
  );
}

function Public ()
{
  return <h3>Public</h3>
}

function Protected(){
  return <h3>Protected</h3>
}

class Login extends React.Component{
  state={
    redirectToReferrer:false
  };
  login =()=>{
    fakeAuth.authenticate(() => {
      this.setState({redirectToReferrer:true});
    });
  };

render(){
  let {from} = this.props.location.state||{from:{pathname:"/"}};
  let {redirectToReferrer}=this.state;
  if( redirectToReferrer) return<Redirect to={from}/>
  return(
 <div>
   <p>You must log in to view the page at{from.pathname}</p>
   <button onClick={this.login}>Log In</button>
 </div>
  );
}
}

export default AppRouterAuth;